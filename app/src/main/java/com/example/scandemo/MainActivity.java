package com.example.scandemo;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView iv = findViewById(R.id.iv);

        //条形码生成方式一  推荐此方法
        BarCodeUtils.builder("https://www.baidu.com")
                .backColor(android.R.color.white)
                .codeColor(Color.BLACK)
                .codeWidth(1000)
                .codeHeight(300)

                .into(iv);
    }
}
